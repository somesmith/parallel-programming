#include <ctime>
#include <cmath>
#include <vector>
#include "stdint.h"
using namespace std;
void wait(DWORD interval){
    DWORD startTime = GetTickCount();
    while(GetTickCount() < (startTime + interval))
    {}
}
void fill_matrix(vector <double> &m, double s) {
    srand(time(0));
    vector <double> cell(17,0);
    double sz=cell.size();
    int max=9;
    int temp=max;
    for(int i=0; i<cell.size(); i++){
        if (i<max) {
            cell[i]=1.0/temp;
            temp--;
        }
        if (i>=max) {
            cell[i]=1.0*(i-(max-2));
        }
    }
    cout<<"Mnozhyna znachen klitynok matrytsi:"<<endl;
    for(double i=0; i<cell.size(); i++) {
        cout<<i<<": "<<cell[i]<<endl;
    }
    for (int i = 0; i<s; i++){
        m.push_back(cell[rand()%cell.size()]);
    }
}
void pretty_print(const vector<double>& c) {
    static int count = 1;
    cout << (count) << "--";
    for (int i = 0; i < c.size(); ++i) {
        cout << c[i] << " ";
    }
    count++;
}
void find_ind(double k, double left, double length, double count, vector <vector <double> > &index) {
    if (k >= left && k<left + length){
        vector <double> row;
        row.push_back(count);
        double shift = k - left;
        row.push_back(count + shift);
        index.push_back(row);
        return;
    }
    else find_ind(k, left + length, length - 1, count + 1, index);
}
bool check(vector <vector<double> > &comb, double size) {
    vector <bool> checked(size, false);
    int i = 0;
    for (double number = 0; number<size && i<size;) {
        if (comb[i][0] == number){
            checked[number] = true;
            number++;
        }
        if (comb[i][1] == number){
            checked[number] = true;
            number++;
        }
        i++;
    }
    for (int number = 0; number<size; number++) {
        if (!checked[number]) return false;
    }
    return true;
}
void outputVector(const vector <double> &v, string text) {
    cout << endl << (text + ": ");
    for (double i = 0; i<v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}
void comb(double offset, double k, vector <double> &m, vector <double> &combination, double len, vector<double> &arithW, vector<double> &geomW, double &counter) {
    if (k == 0) {
        vector <vector <double> > ind;
        pretty_print(combination);
        for (int i = 0; i<combination.size(); i++) {
            find_ind(combination[i], 0, len, 0, ind);
        }
        vector <double> full_comb(m.size(), -1);
        for (int i = 0; i<combination.size(); i++) {
            int index = combination[i];
            full_comb[index] = m[index];
        }
        vector <double> indices(combination);
        bool ch = check(ind, len);
        if (ch) {
            counter++;
            bool flag;
            do {
                flag = false;
                for (double i = 0; i < full_comb.size(); i++) {
                    if (full_comb[i] == -1) {
                        find_ind(i, 0, len, 0, ind);
                        double index_i = ind[ind.size() - 1][0];
                        double index_j = ind[ind.size() - 1][1];
                        ind.pop_back();
                        double p1_index, p2_index;
                        double index_k;
                        bool present_a_ik = false;
                        for (int pair = 0; pair < ind.size(); pair++) {
                            if (ind[pair][0] == index_i && ind[pair][1] != index_j) {
                                present_a_ik = true;
                                index_k = ind[pair][1];
                                p1_index = pair;
                            }
                        }
                        if (present_a_ik) {
                            bool present_a_tk = false;
                            for (double pair = 0; pair < ind.size(); pair++) {
                                if (ind[pair][0] != index_i && ind[pair][1] == index_k) {
                                    present_a_tk = true;
                                    p2_index = pair;
                                }
                            }
                            if (present_a_tk) {
                                full_comb[i] = full_comb[indices[p1_index]] / full_comb[indices[p2_index]] * 1.0;
                                indices.push_back(i);
                                vector <double> new_pair;
                                new_pair.push_back(index_i);
                                new_pair.push_back(index_j);
                                ind.push_back(new_pair);
                                flag = true;
                            }
                            else
                            {
                                bool present_a_tj = false;
                                for (int pair = 0; pair < ind.size(); pair++) {
                                    if (ind[pair][0] != index_i && ind[pair][1] == index_j) {
                                        present_a_tj = true;
                                        p2_index = pair;
                                    }
                                }
                                if (present_a_tj) {
                                    full_comb[i] = full_comb[indices[p1_index]] * full_comb[indices[p2_index]] * 1.0;
                                    indices.push_back(i);
                                    vector <double> new_pair;
                                    new_pair.push_back(index_i);
                                    new_pair.push_back(index_j);
                                    ind.push_back(new_pair);
                                    flag = true;
                                }
                            }
                        }
                        else
                        {
                            bool present_a_kj = false;
                            for (double pair = 0; pair < ind.size(); pair++) {
                                if (ind[pair][0] != index_i && ind[pair][1] == index_j) {
                                    present_a_kj = true;
                                    index_k = ind[pair][0];
                                    p1_index = pair;
                                }
                            }
                            if (present_a_kj) {
                                bool present_a_kt = false;
                                for (int pair = 0; pair < ind.size(); pair++) {
                                    if (ind[pair][0] == index_k && ind[pair][1] != index_j) {
                                        present_a_kt = true;
                                        p2_index = pair;
                                    }
                                }
                                if (present_a_kt) {
                                    full_comb[i] = full_comb[indices[p1_index]] / full_comb[indices[p2_index]] * 1.0;
                                    indices.push_back(i);
                                    vector <double> new_pair;
                                    new_pair.push_back(index_i);
                                    new_pair.push_back(index_j);
                                    ind.push_back(new_pair);
                                    flag = true;
                                }
                                else {
                                    bool present_a_it = false;
                                    for (double pair = 0; pair < ind.size(); pair++) {
                                        if (ind[pair][0] == index_i && ind[pair][1] != index_j) {
                                            present_a_it = true;
                                            p2_index = pair;
                                        }
                                    }
                                    if (present_a_it) {
                                        full_comb[i] = full_comb[indices[p1_index]] * full_comb[indices[p2_index]] * 1.0;
                                        indices.push_back(i);
                                        flag = true;
                                        vector <double> new_pair;
                                        new_pair.push_back(index_i);
                                        new_pair.push_back(index_j);
                                        ind.push_back(new_pair);
                                    }
                                }
                            }
                        }
                    }
                }
            } while (flag);
            outputVector(full_comb, "Result");
            double sum_of_row = 0;
            for (int i = 0; i<len; i++) {
                sum_of_row += full_comb[i];
            }
            sum_of_row += 1;
            double real_size = len + 1;
            vector <double> weights(real_size);
            weights[0] = 1.0 / sum_of_row;
            arithW[0] += 1.0 / sum_of_row;
            geomW[0] *= 1.0 / sum_of_row;
            for (double i = 1; i<real_size; i++) {
                weights[i] = full_comb[i] / sum_of_row*1.0;
                arithW[i] += weights[i];
                geomW[i] *= weights[i];
            }
            outputVector(arithW, "Weight");
            cout << endl << endl;
        }
        else cout << "Canʼt take this numbers" << endl;
        return;
    }
    for (int i = offset; i <= m.size() - k; ++i) {
        combination.push_back(i);
        comb(i + 1, k - 1, m, combination, len, arithW, geomW, counter);
        combination.pop_back();
    }
}
int main() {
    int n;
    cin >> n;
    int size = n*(n - 1) / 2;
    vector <double> elements;
    fill_matrix(elements, size);
    outputVector(elements, "");
    vector<double> subset;
    vector<double> arithmetic_weights(n, 0);
    vector<double> geometric_weights(n, 1);
    double numberOfcomb = 0;
    int t1=clock();
    DWORD dt1=GetTickCount();
    comb(0, n-1 , elements, subset, n-1 , arithmetic_weights, geometric_weights, numberOfcomb);
    int t2=clock();
    DWORD dt2=GetTickCount();
    int time1=t2-t1;
    DWORD dtime1=dt2-dt1;
    for (int i = 0; i<arithmetic_weights.size(); i++) {
        arithmetic_weights[i] /= numberOfcomb;
    }
    for (int i = 0; i<geometric_weights.size(); i++) {
        pow(geometric_weights[i], 1 / numberOfcomb);
    }
    cout<<endl<<endl;
    cout << "Prioritis" << endl;
    outputVector(arithmetic_weights, "");
    cout<<"Count of comb: "<<numberOfcomb<<endl;
    return 0;
}
