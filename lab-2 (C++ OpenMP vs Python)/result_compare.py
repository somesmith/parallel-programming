import pickle
import numpy as np
import matplotlib.pyplot as plt

def PrintResult(result):
	print("\nRESULTS\n")
	for i in result:
		print(	"N = ", i["n"], 
				"\tLine time = %.3f \tParallel time = %.3f \tKKD = %.3f"
			 	% (i["line_time"], i["parallel_time"], i["kkd"]))


def Serialization(object, filename):
	with open(filename, 'wb') as f:
		pickle.dump(result, f)

def Deserialization(filename):
	with open(filename, 'rb') as f:
		result = pickle.load(f)
	return result

def ReadCppResults(filename):
	result = []
	with open(filename, 'r') as f:
		for line in f:
			words = line.split()
			tmp = {}
			tmp["N"] = int(words[0])
			tmp["Default"] = float(words[1])
			tmp["Parallel"] = float(words[2])
			tmp["Section"] = float(words[3])
			tmp["HalfSection"] = float(words[4])
			tmp["FullSection"] = float(words[5])
			result.append(tmp)
			print(tmp)
	return result

def DrawPlot(result, cppresults):
	n = []
	line_points = []
	parallel_points = []

	for i in result:
		n.append(i['n'])
		line_points.append(i['line_time'])
		parallel_points.append(i['parallel_time'])

	# line_plot = plt.plot(n, line_points, label='python: default linear', color = 'b', linewidth=2)
	# print('Plot: ', len(line_plot), line_plot)

	# parallel_plot = plt.plot(n, parallel_points, label='python: parallel linear', color = 'y', linewidth=2)
	# print('Plot: ', len(parallel_plot), parallel_plot)

############### cpp results ############
	N = []
	Default = []
	Parallel = []
	Section = []
	HalfSection = []
	FullSection = []
	
	for i in cppresults:
		N.append(i["N"])
		Default.append(i["Default"])
		Parallel.append(i["Parallel"])
		Section.append(i["Section"])
		HalfSection.append(i["HalfSection"])
		FullSection.append(i["FullSection"])

	Default_plot = plt.plot(N, Default, label="C++: Default", color = 'r', linewidth=1)
	print('Plot: ', len(Default_plot), Default_plot)

	Parallel_plot = plt.plot(N, Parallel, label='C++: Parallel', color = 'c', linewidth=1)
	print('Plot: ', len(Parallel_plot), Parallel_plot)

	Section_plot = plt.plot(N, Section, label='C++: Section', color = 'm', linewidth=1)
	print('Plot: ', len(Section_plot), Section_plot)

	HalfSection_plot = plt.plot(N, HalfSection, label='C++: HalfSection', color = 'g', linewidth=1)
	print('Plot: ', len(HalfSection_plot), HalfSection_plot)

	FullSection_plot = plt.plot(N, FullSection, label='C++: FullSection', color = 'k', linewidth=1)
	print('Plot: ', len(FullSection_plot), FullSection_plot)

	plt.legend()

	plt.title("Comparing")
	plt.xlabel('Size of matrix')
	plt.ylabel('Time to solve, sec')

	grid1 = plt.grid(True)
	plt.show()



if __name__ == "__main__":

	filename = 'lab_1.pickle'
	cppfilename = 'ompresults.txt'
	
	result = Deserialization(filename)
	PrintResult(result)
	cppresults = ReadCppResults(cppfilename)

	DrawPlot(result, cppresults)
