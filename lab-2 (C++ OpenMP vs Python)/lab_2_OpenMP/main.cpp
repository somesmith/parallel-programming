#include <iostream>
#include <omp.h>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <iomanip>
#include <fstream>

#define ranged_for(var, min, max, step) for(auto var = (min); var < (max); var += (step) )

using namespace std;

void printMatrix(int **matrix, int N, int M) {
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++) {
            cout << matrix[i][j] << "\t";
        }
        cout << endl;
    }
}

void fillMatrix(int** matrix, int N, int M) {

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++) {
            matrix[i][j] = rand() % 1000;
        }
    }
}

void createMatrix(int** matrix, int N, int M) {

    for (int i = 0; i < N; i++) {
        matrix[i] = new int[M];
    }
}

void deleteMatrix(int** matrix, int N) {
    for (int i = 0; i < N; i++) {
        delete[] matrix[i];
    }
    delete[]matrix;
}

double defaultMultiplication(int** matrixA, int** matrixB, int** resultMatrixDefault, int N, int M, int K){
    double st = omp_get_wtime();
    int i, j, k;
    for (i = 0; i < N; i++) {
        for (j = 0; j < K; j++)
        {
            for (k = 0; k < M; k++)
            {
                resultMatrixDefault[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }

    double en = omp_get_wtime();
    return (en-st);
}

double ParallelMultiplication(int** matrixA, int** matrixB, int** resultMatrixParallel, int N, int M, int K){
    int thread_num = omp_get_max_threads();
    int i, j, k;
    double st = omp_get_wtime();

    # pragma omp parallel for  private(i,j,k) num_threads(thread_num)
    for (i = 0; i < N; i++) {
        for (j = 0; j < K; j++)
        {
            for (k = 0; k < M; k++)
            {
                resultMatrixParallel[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }
    double en = omp_get_wtime();
    return (en-st);
}

double SectionMultiplication(int** matrixA, int** matrixB, int** resultMatrixSection, int N, int M, int K){
    double st = omp_get_wtime();
    int i, j, k;
    int thread_num = omp_get_max_threads();
    int threads = (thread_num/4);
    cout << "Treads = " << threads << endl;
    int n = N/threads;
    cout << "N = " << N << endl;
    cout << "n = " << n << endl;
    #pragma omp parallel
    {
        #pragma omp sections private(i,j,k)
        {
            #pragma omp section
            {
                for (i = 0; i < n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }
            #pragma omp section
            {
                for (i = n; i < N; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }
        }
    }

    double en = omp_get_wtime();
    return (en-st);
}

double HalfSectionMultiplication(int** matrixA, int** matrixB, int** resultMatrixSection, int N, int M, int K){
    double st = omp_get_wtime();
    int thread_num = omp_get_max_threads();
    int i, j, k;
    int threads = (thread_num/2);
    cout << "Treads = " << threads << endl;
    int n = N/threads;
    cout << "N = " << N << endl;
    cout << "n = " << n << endl;
    #pragma omp parallel num_threads(thread_num)
    {
        #pragma omp sections private(i,j,k)
        {
            #pragma omp section
            {
                for (i = 0; i < n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = n; i < 2*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 2*n; i < 3*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 3*n; i < N; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }
        }
    }

    double en = omp_get_wtime();
    return (en-st);
}

double FullSectionMultiplication(int** matrixA, int** matrixB, int** resultMatrixSection, int N, int M, int K){
    double st = omp_get_wtime();
    int thread_num = omp_get_max_threads();
    int i, j, k;
    cout << "Treads = " << thread_num << endl;
    int n = N/thread_num;
    cout << "N = " << N << endl;
    cout << "n = " << n << endl;
    #pragma omp parallel num_threads(thread_num)
    {
        #pragma omp sections private(i,j,k)
        {
            #pragma omp section
            {
                for (i = 0; i < n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = n; i < 2*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 2*n; i < 3*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 3*n; i < 4*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 4*n; i < 5*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 5*n; i < 6*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 6*n; i < 7*n; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }

            #pragma omp section
            {
                for (i = 7*n; i < N; i++) {
                    for (j = 0; j < K; j++){
                        for (k = 0; k < M; k++){
                            resultMatrixSection[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
            }
        }
    }

    double en = omp_get_wtime();
    return (en-st);
}

void compare(int N, int M, int K, vector <map <string,double>>* resultComparing){
    int** matrixA = new int* [N];
    int** matrixB = new int* [M];
    int** resultMatrixParallel = new int* [N];
    int** resultMatrixDefault = new int* [N];
    int** resultMatrixSection = new int* [N];
    int** resultMatrixHalfSection = new int* [N];
    int** resultMatrixFullSection = new int* [N];

    //create result matrix
    for (int i = 0; i < N; i++) {
        resultMatrixDefault[i] = new int[K];
        resultMatrixParallel[i] = new int[K];
        resultMatrixSection[i] = new int[K];
        resultMatrixHalfSection[i] = new int[K];
        resultMatrixFullSection[i] = new int[K];
    }

    //fill zeros ResultMatrix
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < K; j++)
        {
            resultMatrixParallel[i][j] = 0;
            resultMatrixDefault[i][j] = 0;
            resultMatrixSection[i][j] = 0;
            resultMatrixHalfSection[i][j] = 0;
            resultMatrixFullSection[i][j] = 0;
        }
    }

    createMatrix(matrixA, N, M);
    fillMatrix(matrixA, N, M);

    createMatrix(matrixB, M, K);
    fillMatrix(matrixB, M, K);

    cout << "Start\n";
    double defaultTimeResult = defaultMultiplication(matrixA, matrixB, resultMatrixDefault, N, M, K);
    printf("Default multiplication time %lf\n", defaultTimeResult);
    double ParallelTimeResult = ParallelMultiplication(matrixA, matrixB, resultMatrixParallel, N, M, K);
    printf("Parallel multiplication time %lf\n", ParallelTimeResult);
    double SectionTimeResult = SectionMultiplication(matrixA, matrixB, resultMatrixSection, N, M, K);
    printf("Section multiplication time %lf\n", SectionTimeResult);
    double HalfSectionTimeResult = HalfSectionMultiplication(matrixA, matrixB, resultMatrixHalfSection, N, M, K);
    printf("HalfSection multiplication time %lf\n", HalfSectionTimeResult);
    double FullSectionTimeResult = FullSectionMultiplication(matrixA, matrixB, resultMatrixFullSection, N, M, K);
    printf("FullSection multiplication time %lf\n", FullSectionTimeResult);
    cout << "End!\n";
    bool isEqualMatrix = true;

    for (int i = 0; i < N && isEqualMatrix; i++)
    {
        for (int j = 0; j < K && isEqualMatrix; j++)
        {
            if (resultMatrixParallel[i][j] != resultMatrixDefault[i][j]     &&
                resultMatrixSection[i][j] != resultMatrixDefault[i][j]      &&
                resultMatrixFullSection[i][j] != resultMatrixDefault[i][j]  &&
                resultMatrixHalfSection[i][j] != resultMatrixDefault[i][j])
            {
                isEqualMatrix = false;
                cout << "MATRIX ARE NOT EQUAL!!!" << endl;
                break;
            }
        }
    }
    if (isEqualMatrix){
        cout << "MATRIX ARE EQUAL!" << endl;
        map<string, double> value = {
            {"N", N},
            {"Default", defaultTimeResult},
            {"Parallel", ParallelTimeResult},
            {"Section", SectionTimeResult},
            {"HalfSection", HalfSectionTimeResult},
            {"FullSection", FullSectionTimeResult}
        };
        resultComparing->push_back(value);
    }

    deleteMatrix(matrixA, N);
    deleteMatrix(matrixB, M);
    deleteMatrix(resultMatrixDefault, N);
    deleteMatrix(resultMatrixParallel, N);
    deleteMatrix(resultMatrixSection, N);
    deleteMatrix(resultMatrixHalfSection, N);
    deleteMatrix(resultMatrixFullSection, N);
}

void printVectorOfResults(vector <map <string,double>>* resultComparing){
    for (auto iter=resultComparing->begin(); iter!=resultComparing->end(); iter++){
        cout << endl;
        cout << setprecision(8) << fixed;
        cout << setw(5) << "N = " << setw(4) << (*iter)["N"]
             << setw(10) << "\tDefault = " << setw(8) << (*iter)["Default"]
             << setw(10) << "\tParralel = " << setw(8) << (*iter)["Parallel"]
             << setw(10) << "\tSection = " << setw(8) << (*iter)["Section"]
             << setw(15) << "\tHalfSection = " << setw(8) << (*iter)["HalfSection"]
             << setw(15) << "\tFullSection = " << setw(8) << (*iter)["FullSection"] << endl;
    }
}

void GenerateResultsToFile(string filename, vector <map <string,double>>* resultComparing){
    ranged_for(i, 10, 99, 10){
        compare(i,i,i,resultComparing);
    }
    ranged_for(i, 100, 2001, 100){
        compare(i,i,i,resultComparing);
    }
    ofstream fout;
    fout.open(filename);
    for (auto iter=resultComparing->begin(); iter!=resultComparing->end(); iter++){
        fout << setprecision(4) << fixed;
        fout << setw(4) << " " << int((*iter)["N"])
             << setw(8) << " " << (*iter)["Default"]
             << setw(8) << " " << (*iter)["Parallel"]
             << setw(8) << " " << (*iter)["Section"]
             << setw(8) << " " << (*iter)["HalfSection"]
             << setw(8) << " " << (*iter)["FullSection"];
        fout << endl;
    }
    fout.close();
}


int main()
{
    vector <map <string,double>>* resultComparing = new vector <map <string,double>>();
    cout <<"VERSION OPENMP: \t"<< _OPENMP << endl;
    int thread_num = omp_get_max_threads();
    cout << "COUNT OF THREADS: " << thread_num << endl;

//    int N, M, K;
//    cout << "Input N: ";
//    cin >> N;
//    M = N;
//    K = N;
//    cout << "Input M: ";
//    cin >> M;
//    cout << "Input K: ";
//    cin >> K;
//    compare(M,N,K,resultComparing);

    GenerateResultsToFile("../ompresults.txt", resultComparing);

    printVectorOfResults(resultComparing);
    return 0;
}
