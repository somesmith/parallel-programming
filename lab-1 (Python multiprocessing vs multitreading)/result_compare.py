import pickle
import numpy as np
import matplotlib.pyplot as plt

from multiprocess import Compare, PrintResult

def Serialization(object, filename):
	with open(filename, 'wb') as f:
		pickle.dump(result, f)

def Deserialization(filename):
	with open(filename, 'rb') as f:
		result = pickle.load(f)
	return result

def Comparing():
	result = []
	for i in range(10,100,10):
		print("#"*50, "Iteration n = ", i, "#"*50)
		tmp = compare(i,i,i)
		result.append(tmp)
	for i in range(100,1001,100):
		tmp = compare(i,i,i)
		result.append(tmp)

	return result

def DrawPlot(result):
	n = []
	line_points = []
	parallel_points = []

	for i in result:
		n.append(i['n'])
		line_points.append(i['line_time'])
		parallel_points.append(i['parallel_time'])

	line_plot = plt.plot(n, line_points)
	print('Plot: ', len(line_plot), line_plot)

	parallel_plot = plt.plot(n, parallel_points)
	print('Plot: ', len(parallel_plot), parallel_plot)

	plt.title("Comparing")
	plt.xlabel('Size of matrix')
	plt.ylabel('Time to solve, sec')

	grid1 = plt.grid(True)   # линии вспомогательной сетки
	plt.show()



if __name__ == "__main__":

	filename = 'lab_1.pickle'

#	result = Comparing()

#	Serialization(result, filename)
	
	result = Deserialization(filename)

	PrintResult(result)

	DrawPlot(result)