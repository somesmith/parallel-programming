import random
import numpy as np
import math
import multiprocessing
from multiprocessing import Process, Manager
from threading import Thread
from timeit import default_timer as timer

def MatrixInit(n, m, k):
	matrixA = [[random.randint(0,10) for i in range(m)] for j in range(n)]
	matrixB = [[random.randint(0,10) for k in range(k)] for l in range(m)]
	return matrixA, matrixB

def ParallelMultiplication(row, rowMatrixA, MatrixB, result):
	result_row = []
	for i in range(0,len(MatrixB[0])):
		tmp = 0
		for j in range(0,len(rowMatrixA)):
			tmp += rowMatrixA[j]*MatrixB[j][i]
		result_row.append(tmp)
	result[row] = result_row

def Threading(start_row, end_row, matrixA, matrixB, result):
	jobs = []
	for row in range(start_row, end_row):
		p = Thread(target=ParallelMultiplication, args=(row,matrixA[row],matrixB,result))
		jobs.append(p)
		p.start()

	for jober in jobs:
		jober.join()

def  multipleMatrix(matrixA,matrixB):
	n = len(matrixA)
	m = len(matrixB)
	k = len(matrixB[0])
	result = []
	if m == len(matrixA[0]):
		for i in range(0,n):
			row = []
			for j in range(0,k):
				tmp = 0
				for M in range(0,m):
					tmp += matrixA[i][M]*matrixB[M][j]
				row.append(tmp)
			result.append(row)
	return result

def Compare(n,m,k):
	print("\n#####   Initialization   #####\n")

	matrixA, matrixB = MatrixInit(n, m, k)

	print("Matrix A:\n", np.array(matrixA))
	print("Matrix B:\n", np.array(matrixB))

################################ пряме множення ###################################

	print("\n#####   linear method   #####\n")
	start = timer()

	line_result = np.array(multipleMatrix(matrixA,matrixB))

	end = timer()

	line_time = end - start

	print("Time for linear method: ", line_time)
	print("Result matrix of linear method:\n", line_result)

################################ паралельне множення ###################################	

	print("\n#####   Parallel method   #####\n")

	cores = multiprocessing.cpu_count()
	tasks_on_core = math.ceil(len(matrixA)/cores)

	print("cores: ", cores)
	print("tasks_on_core: ", tasks_on_core)

	start = timer()

	process_jobs = []
	with Manager() as manager:
		proxy_list = manager.list(range(n))
		for core in range(0,cores): # разперделяем наборы задач по ядрам
			start_row = core*tasks_on_core
			end_row = (core+1)*tasks_on_core
			if end_row > len(matrixA):
				end_row = len(matrixA)

			p = Process(target=Threading, args=(start_row, end_row, matrixA, matrixB, proxy_list))
			process_jobs.append(p)
			p.start()

		for jober in process_jobs:
			jober.join()
		parallel_result = np.array(proxy_list)

	end = timer()

	parallel_time = end - start

	print("Time for parallel method: ", parallel_time)
	print("Result matrix of parallel method:\n", parallel_result)

	comparison = line_result == parallel_result
	equals = comparison.all()
	kkd = line_time/parallel_time
	if equals:
	    print("Matrix are equal")
	    return {'n':n, 'm':m, 'k':k, 'line_time': line_time, 'parallel_time': parallel_time, 'kkd': kkd}
	else:
	    print("Matrix are not equal")

def PrintResult(result):
	print("\nRESULTS\n")
	for i in result:
		print(	"N = ", i["n"], 
				"\tLine time = %.3f \tParallel time = %.3f \tKKD = %.3f"
			 	% (i["line_time"], i["parallel_time"], i["kkd"]))


if __name__ == "__main__":
	n = int(input("Input n (rows of matrix A): "))
	m = int(input("Input m (cols of matix A and rows of matrix B): "))
	k = int(input("Input k (cols of matrix B): "))
	result = []
	result.append(Compare(n, m, k))
	PrintResult(result)