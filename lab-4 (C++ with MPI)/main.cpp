#include <stdio.h>
#include "mpi.h"
#include <iostream>
#define N 10
#define M 10
using namespace std;
void printMatrix(int** matrix) {
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++) {
            cout << matrix[i][j] << "\t";
        }
        cout << endl;
    }
}
void fillMatrix(int** matrix) {

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++) {
            matrix[i][j] = (-100) + rand() % 200;
        }
    }
}
void createMatrix(int** matrix) {

    for (int i = 0; i < N; i++) {
        matrix[i] = new int[M];
    }
}
void deleteMatrix(int** matrix) {
    for (int i = 0; i < N; i++) {
        delete[] matrix[i];
    }
    delete[]matrix;
}



int main(int argc, char* argv[]) {


    int rank, size;
    int GlobalCount;
    double start, end, time, sumTime = 0;
    MPI_Status Status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);



    start = MPI_Wtime();
    if (rank == 0) {
        cout << "Ivanov Victor KN-31 variant-7" << endl;


        int** matrix = new int* [N];
        createMatrix(matrix);
        fillMatrix(matrix);
        printMatrix(matrix);

        int portion = N / size;
        int localCount = 0;
        int zalyshok = N % size;
        int finalPortion = 0;
        int i = 0;
        int prev = 0;
        int nomer = 0;
        int sum;
        for (i = 1; i < size; i++) {
            finalPortion = portion + (zalyshok-- > 0 ? 1 : 0);
            MPI_Send(&finalPortion, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            for (int j = 0; j < finalPortion; j++) {
                nomer = prev + j;
                MPI_Send(&nomer, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                MPI_Send(matrix[nomer], M, MPI_INT, i, 0, MPI_COMM_WORLD);
            }
            prev += finalPortion;

        }
        for (i = prev; i < N; i++) {
            sum = 0;
            for (int j = 0; j < M; j++) {
                if (matrix[i][j] > 0){
                    sum += matrix[i][j];
//                    cout << "row - " << i + 1 << " matrix[i][j]: " << matrix[i][j] << endl;
                }
            }
            cout << "Row: " << i + 1 << "; Sum of positive elements = " << sum << " \t , Rank = " << rank << endl;
            localCount += sum;
        }
        MPI_Reduce(&localCount, &GlobalCount, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    }
    else {
        int finalPortion, nomer;
        int pointer[M];
        MPI_Recv(&finalPortion, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
        int sum;
        int localCount = 0;
        for (int i = 0; i < finalPortion; i++) {
            MPI_Recv(&nomer, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
            MPI_Recv(pointer, M, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
            sum = 0;
            for (int j = 0; j < M; j++) {
                if (pointer[j] > 0)
                    sum += pointer[j];
            }
            cout << "Row: " << i + 1 << "; Sum of positive elements = " << sum << " \t , Rank = " << rank << endl;
            localCount += sum;
        }
        MPI_Reduce(&localCount, &GlobalCount, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    end = MPI_Wtime();
    MPI_Barrier(MPI_COMM_WORLD);
    time = end - start;
    cout << "Execution time of process number " << rank << " -Time : " << time << endl;
    MPI_Reduce(&time, &sumTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        cout << "All time - " << sumTime << endl;
        cout << "Global Reduction - " << GlobalCount << endl;
    }
    MPI_Finalize();


    return 0;
}
